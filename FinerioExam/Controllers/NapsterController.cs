﻿
using Napster.Models;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScrapySharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Text.Json;

namespace Napster.Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NapsterController : ControllerBase
    {
        HtmlWeb oSitio = new HtmlWeb();
        private readonly String sMainPageUrl = "https://us.napster.com/";
        private readonly ILogger<NapsterController> _logger;


        public NapsterController(ILogger<NapsterController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Genero> GetGeneros()
        {
            
            HtmlDocument pageGeneros = oSitio.Load(sMainPageUrl + "music");
            List<Genero> generos = new List<Genero>();
            foreach (HtmlNode nGenre in pageGeneros.DocumentNode.CssSelect(".genre-item"))
            {
                Genero objGenero = new Genero();
                var oA = nGenre.CssSelect("a").FirstOrDefault();
                var oSpan = oA.CssSelect("p").FirstOrDefault().CssSelect("span").FirstOrDefault();

                objGenero.href = oA.Attributes["href"].Value;
                objGenero.nombre = oSpan.InnerText.Replace("\n","");
                objGenero.subgeneros = new List<Subgenero>();

                HtmlDocument pageSubGeneros = oSitio.Load(sMainPageUrl + objGenero.href);
                foreach(HtmlNode nSubGenre in pageSubGeneros.DocumentNode.CssSelect(".genre-item"))
                {
                    Subgenero objSubGenero = new Subgenero();
                    var oASub = nSubGenre.CssSelect("span").FirstOrDefault().CssSelect("a").FirstOrDefault();
                    objSubGenero.href = oASub.Attributes["href"].Value;
                    objSubGenero.nombre = oASub.InnerText.Replace("\n", ""); ;
                    objGenero.subgeneros.Add(objSubGenero);
                }

                generos.Add(objGenero);

            }

            //Se inserta o edita el ultimo registro de generos
            try
            {
                Generos bdGen = new Generos();
                int id = bdGen.GetFirstID();
                string json = JsonSerializer.Serialize(generos);
                if (id == 0)
                {
                    bdGen.Insert(json);
                }
                else
                {
                    bdGen.Update(json, id);
                }
            }
            catch(Exception ex) { }

            return generos;

        }

        [HttpGet]
        [Route("artistbygenre")]
        public IEnumerable<Artista> GetArtistasGenero(string genero)
        {
            
            HtmlDocument pageGeneros = oSitio.Load(sMainPageUrl+genero);
          
            List<Artista> lstArtistasGenero = new List<Artista>();

            foreach (HtmlNode nArtists in pageGeneros.DocumentNode.CssSelect(".artist-list-grid"))
            {
                foreach (HtmlNode nArtist in nArtists.CssSelect(".artist-item"))
                {
                    Artista objArtist = new Artista();
                    var oA = nArtist.CssSelect(".artist-link").FirstOrDefault();
                    objArtist.link = oA.Attributes["href"].Value;
                    objArtist.nombre = oA.InnerText.Replace("\n", ""); ;

                    lstArtistasGenero.Add(objArtist);

                }

            }

            //Se inserta o edita el ultimo registro de generos
            try
            {
                Artistas bd = new Artistas();
                int id = bd.GetFirstID(genero);
                string json = JsonSerializer.Serialize(lstArtistasGenero);
                if (id == 0)
                {
                    bd.Insert(json,genero);
                }
                else
                {
                    bd.Update(json, id);
                }
            }
            catch (Exception ex) { }

            return lstArtistasGenero;

        }

        [HttpGet]
        [Route("albumsbyartist")]
        public IEnumerable<AlbumGroup> GeAlbumsPorArtista(string artist)
        {

            HtmlDocument pageAlbums = oSitio.Load(sMainPageUrl + artist + "/albums");
            List<AlbumGroup> lstAlbunesArtista = new List<AlbumGroup>();

            foreach (HtmlNode nAlbums in pageAlbums.DocumentNode.CssSelect(".album-group"))
            {
                AlbumGroup objAlbumGroup = new AlbumGroup();
                objAlbumGroup.grupo = nAlbums.CssSelect(".header").FirstOrDefault().CssSelect("h2").FirstOrDefault().InnerText.Replace("\n", "");
                objAlbumGroup.albunes = new List<Album>();
                

                foreach (HtmlNode nAlbum in nAlbums.CssSelect(".album-list").CssSelect(".album-item"))
                {
                    Album objAlbum = new Album();
                    HtmlNode oAlbumLink = nAlbum.CssSelect(".album-link").FirstOrDefault();
                    if (oAlbumLink != null)
                    {
                        objAlbum.href = oAlbumLink.Attributes["href"].Value;
                        objAlbum.nombre = oAlbumLink.InnerText.Replace("\n", "");
                    }
                    HtmlNode oArtistLink = nAlbum.CssSelect(".artist-link").FirstOrDefault();
                    if(oArtistLink!=null)
                        objAlbum.nombre_artista = oArtistLink.InnerText.Replace("\n", "");
               
                   
                    objAlbumGroup.albunes.Add(objAlbum);

                }
                lstAlbunesArtista.Add(objAlbumGroup);
            }

            //Se inserta o edita el ultimo registro de generos
            try
            {
                Albums bd = new Albums();
                int id = bd.GetFirstID(artist);
                string json = JsonSerializer.Serialize(lstAlbunesArtista);
                if (id == 0)
                {
                    bd.Insert(json,artist);
                }
                else
                {
                    bd.Update(json, id);
                }
            }
            catch (Exception ex) { }

            return lstAlbunesArtista;

        }

        [HttpGet]
        [Route("songsbyalbum")]
        public IEnumerable<Cancion> GetCancionesAlbum(string album)
        {

            HtmlDocument pageAlbum = oSitio.Load(sMainPageUrl + album);
            List<Cancion> lstCancionesAlbum = new List<Cancion>();

            foreach (HtmlNode nCancion in pageAlbum.DocumentNode.CssSelect(".track-item"))
            {
                Cancion objCancion = new Cancion();
                HtmlNode nTrack = nCancion.CssSelect(".track-title").FirstOrDefault();
                if(nTrack != null)
                {
                    objCancion.nombre = nTrack.InnerText.Replace("\n", "");
                    objCancion.href = nTrack.Attributes["href"].Value;
                    lstCancionesAlbum.Add(objCancion);
                }
            }

            //Se inserta o edita el ultimo registro de generos
            try
            {
                Canciones bd = new Canciones();
                int id = bd.GetFirstID(album);
                string json = JsonSerializer.Serialize(lstCancionesAlbum);
                if (id == 0)
                {
                    bd.Insert(json, album);
                }
                else
                {
                    bd.Update(json, id);
                }
            }
            catch (Exception ex) { }

            return lstCancionesAlbum;

        }

        [HttpGet]
        [Route("artistbysong")]
        public string GetArtistaCancion(string track)
        {
            String result = "";
            HtmlDocument pageAlbum = oSitio.Load(sMainPageUrl + track);
            HtmlNode nInfoCancion = pageAlbum.DocumentNode.CssSelect(".title-description-container").FirstOrDefault();
            if(nInfoCancion != null)
            {
                HtmlNode nArtista = nInfoCancion.CssSelect(".artist-link").FirstOrDefault();
                if (nArtista != null)
                {
                    result = nArtista.InnerText.Replace("\n", "");
                }
            }
            return result;
        }

    }
}
