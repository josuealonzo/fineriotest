﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Napster.Models
{
    public class Subgenero
    {
        public string href { get; set; }
        public string nombre { get; set; }
    }
}
