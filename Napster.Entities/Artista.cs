﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Napster.Models
{
    public class Artista
    {
        public string link { get; set; }
        public string nombre { get; set; }
        
    }
    public class Artistas
    {
        static string currentDirectory = Environment.CurrentDirectory;
        static string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + currentDirectory + @"\DataBase\DBNapster.mdf;Integrated Security=True;Connect Timeout=30";
        SqlConnection con;

        public Artistas()
        {
            con = new SqlConnection(ConnectionString);
        }

        public int GetFirstID(string genero)
        {
            int res = 0;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Artistas where Genero = @GENERO", con);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@GENERO", genero);
            da.SelectCommand.Parameters.AddRange(param);
            da.Fill(ds);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    res = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"].ToString());
                }
            }

            return res;

        }

        public bool Insert(string JsonInfo, string genero)
        {
            try
            {

                SqlCommand com = new SqlCommand("INSERT INTO Artistas (Genero,Artistas,Last_update) VALUES(@Genero,@Artistas,GETDATE())", con);
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Genero", genero);
                param[1] = new SqlParameter("@Artistas", JsonInfo);
                com.Parameters.AddRange(param);
                com.Connection.Open();
                int affected = com.ExecuteNonQuery();
                com.Connection.Close();
                return affected > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public bool Update(string JsonInfo, int id)
        {
            try
            {

                SqlCommand com = new SqlCommand("UPDATE Artistas SET Artistas=@Artistas,Last_update=GETDATE()) WHERE Id=@ID", con);
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Artistas", JsonInfo);
                param[1] = new SqlParameter("@ID", id);
                com.Parameters.AddRange(param);
                com.Connection.Open();
                int affected = com.ExecuteNonQuery();
                com.Connection.Close();
                return affected > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

    }
}
