﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Napster.Models
{
    public class Cancion
    {
        public string href { get; set; }
        public string nombre { get; set; }
    }

    public class Canciones
    {
        static string currentDirectory = Environment.CurrentDirectory;
        static string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + currentDirectory + @"\DataBase\DBNapster.mdf;Integrated Security=True;Connect Timeout=30";
        SqlConnection con;

        public Canciones()
        {
            con = new SqlConnection(ConnectionString);
        }

        public int GetFirstID(string album)
        {
            int res = 0;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Canciones where Album = @Album", con);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@Album", album);
            da.SelectCommand.Parameters.AddRange(param);
            da.Fill(ds);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    res = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"].ToString());
                }
            }

            return res;

        }

        public bool Insert(string JsonInfo, string album)
        {
            try
            {

                SqlCommand com = new SqlCommand("INSERT INTO Canciones (Album,Canciones,Last_update) VALUES(@Album,@Canciones,GETDATE())", con);
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Album", album);
                param[1] = new SqlParameter("@Canciones", JsonInfo);
                com.Parameters.AddRange(param);
                com.Connection.Open();
                int affected = com.ExecuteNonQuery();
                com.Connection.Close();
                return affected > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public bool Update(string JsonInfo, int id)
        {
            try
            {

                SqlCommand com = new SqlCommand("UPDATE Canciones SET Canciones=@Canciones,Last_update=GETDATE()) WHERE Id=@ID", con);
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Canciones", JsonInfo);
                param[1] = new SqlParameter("@ID", id);
                com.Parameters.AddRange(param);
                com.Connection.Open();
                int affected = com.ExecuteNonQuery();
                com.Connection.Close();
                return affected > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
    }
}
