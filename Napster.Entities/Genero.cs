﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Napster.Models
{
    public class Genero
    {
        public string href { get; set; }
        public string nombre { get; set; }
        public List<Subgenero> subgeneros { get; set; }

    }

    public  class Generos
    {
        static string currentDirectory = Environment.CurrentDirectory;
        static string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + currentDirectory + @"\DataBase\DBNapster.mdf;Integrated Security=True;Connect Timeout=30";
        SqlConnection con;

        public Generos()
        {
            con = new SqlConnection(ConnectionString);
        }

        public int GetFirstID()
        {
            int res = 0;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Generos", con);
            da.Fill(ds);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    res = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"].ToString());
                }
            }

            return res;

        }

        public bool Insert(string JsonInfo)
        {
            try
            {
               
                SqlCommand com = new SqlCommand("INSERT INTO Generos (Generos,Last_update) VALUES(@GENEROS,GETDATE())", con);
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@GENEROS", JsonInfo);
                com.Parameters.AddRange(param);
                com.Connection.Open();
                int affected = com.ExecuteNonQuery();
                com.Connection.Close();
                return affected > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public bool Update(string JsonInfo,int id)
        {
            try
            {
                
                SqlCommand com = new SqlCommand("UPDATE Generos SET Generos=@GENEROS,Last_update=GETDATE()) WHERE Id=@ID", con);
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@GENEROS", JsonInfo);
                param[1] = new SqlParameter("@ID", id);
                com.Parameters.AddRange(param);
                com.Connection.Open();
                int affected = com.ExecuteNonQuery();
                com.Connection.Close();
                return affected > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

    }
}
