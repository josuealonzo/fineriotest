﻿using Napster.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace NapsterConsole
{
    class Program
    {
        static string url_base = "http://localhost:58339/napster";
        static Genero selectedGenero;
        static Artista selectedArtista;
        static Album selectedAlbum;
        static Cancion selectedCancion;

        static async Task Main(string[] args)
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("                 NAPSTER                ");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine();

            await showGeneros();

        }

        static async Task showGeneros()
        {
            try
            {
                Console.WriteLine("----- Lista de géneros musicales -----");

                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url_base);

                String generos = await response.Content.ReadAsStringAsync();
                List<Genero> lstGeneros = JsonSerializer.Deserialize<List<Genero>>(generos);
                
                
                int cont = 1;
                foreach (Genero genero in lstGeneros)
                {
                    Console.WriteLine(String.Format("{0}-{1}", cont++, genero.nombre));
                    Console.WriteLine(" --Subgeneros");
                    foreach (Subgenero subgenero in genero.subgeneros)
                    {
                        Console.WriteLine(String.Format("   [{0}]", subgenero.nombre));
                    }
                }

                Console.WriteLine(String.Format("[{0}]-{1}", cont++, "SALIR"));

                Console.WriteLine("Escriba el número de género para ver los artistas del género:");
                string opc = Console.ReadLine();
                int respuesta = 0;
                while (!int.TryParse(opc, out respuesta) || respuesta > cont-1)
                {
                    Console.WriteLine("Opción inválida, intente de nuevo");
                    opc = Console.ReadLine();
                }
                if (respuesta == cont - 1)
                {
                    Console.WriteLine("Fin de la aplicación NAPSTER, presione una tecla para salir");
                    Console.ReadLine();
                    Environment.Exit(0);
                }
                else
                {
                    selectedGenero = lstGeneros[Convert.ToInt32(opc) - 1];
                    await showArtistasGenero(selectedGenero);
                }
            }
            catch(Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Ocurrió un error, NAPSTER se cerrará al presionar una tecla");
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        static async Task showArtistasGenero(Genero genero)
        {
            try
            {
                Console.Clear();
                Console.WriteLine("---- Artistas del género " + genero.nombre.ToUpper() + " ----");
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url_base + "/artistbygenre?genero=" + genero.href);

                String artistas = await response.Content.ReadAsStringAsync();
                List<Artista> lstArtistas = JsonSerializer.Deserialize<List<Artista>>(artistas);
                
                int cont = 1;
                foreach (Artista artist in lstArtistas)
                {
                    Console.WriteLine(String.Format("{0}-{1}", cont++, artist.nombre));
                }

                Console.WriteLine(String.Format("[{0}]-{1}", cont++, "REGRESAR"));

                Console.WriteLine("Escriba un número para ver los álbunes del artista:");
                string opc = Console.ReadLine();
                int respuesta = 0;
                while (!int.TryParse(opc, out respuesta) || respuesta > cont - 1)
                {
                    Console.WriteLine("Opción inválida, intente de nuevo");
                    opc = Console.ReadLine();
                }
                if (respuesta == cont - 1)
                {
                    Console.Clear();
                    await showGeneros();
                }
                else
                {
                    selectedArtista = lstArtistas[Convert.ToInt32(opc) - 1];
                    await showAlbumsArtista(selectedArtista);
                }

                
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Ocurrió un error, NAPSTER se cerrará al presionar una tecla");
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }

        }

        static async Task showAlbumsArtista(Artista artista)
        {
            try
            {
                Console.Clear();
                Console.WriteLine("---- Álbunes del artista " + artista.nombre.ToUpper() + " ----");

                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url_base + "/albumsbyartist?artist=" + artista.link);

                String albunes = await response.Content.ReadAsStringAsync();
                List<AlbumGroup> lstAlbunesGroup = JsonSerializer.Deserialize<List<AlbumGroup>>(albunes);
               
                int contAlbum = 1;
                int cont = 1;
                foreach (AlbumGroup albumGroup in lstAlbunesGroup)
                {

                    Console.WriteLine(String.Format("---{0}-{1}", contAlbum++, albumGroup.grupo));
                    cont = 1;
                    foreach (Album album in albumGroup.albunes)
                    {
                        Console.WriteLine(String.Format("    {0}-> {1} [{2}]", cont++, album.nombre, album.nombre_artista));
                    }
                }
                Console.WriteLine(String.Format("[{0}]-{1}", contAlbum++, "REGRESAR"));

                Console.WriteLine("Escriba un número de grupo para ver sus canciones:");
                string opc = Console.ReadLine();
                int respuesta1 = 0;
                while (!int.TryParse(opc, out respuesta1) || respuesta1 > contAlbum - 1)
                {
                    Console.WriteLine("Opción inválida, intente de nuevo");
                    opc = Console.ReadLine();
                }
                if (respuesta1 == contAlbum - 1)
                {
                    Console.Clear();
                    await showArtistasGenero(selectedGenero);
                }
                else
                {
                    AlbumGroup selected = lstAlbunesGroup[Convert.ToInt32(opc) - 1];
                    Console.WriteLine("Escriba un número de álbum para ver sus canciones:");
                    string opc2 = Console.ReadLine();
                    //int respuesta2 = 0;
                    //while (!int.TryParse(opc, out respuesta2) || respuesta2 > cont - 1)
                    //{
                    //    Console.WriteLine("Opción inválida, intente de nuevo");
                    //    opc = Console.ReadLine();
                    //}
                    //if (respuesta1 == contAlbum - 1)
                    //{
                    //    Console.Clear();
                    //    await showGeneros();
                    //}
                    //else
                    {
                        selectedAlbum = selected.albunes[Convert.ToInt32(opc2) - 1];
                        await showCancionesAlbum(selectedAlbum);
                    }
                }

                
                
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Ocurrió un error, NAPSTER se cerrará al presionar una tecla");
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        static async Task showCancionesAlbum(Album album)
        {
            try
            {
                Console.Clear();
                Console.WriteLine("---- Canciones del álbum " + album.nombre.ToUpper() + " ----");

                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url_base + "/songsbyalbum?album=" + album.href);

                String canciones = await response.Content.ReadAsStringAsync();
                List<Cancion> lstCanciones = JsonSerializer.Deserialize<List<Cancion>>(canciones);
                
                int cont = 1;
                foreach (Cancion song in lstCanciones)
                {
                    Console.WriteLine(String.Format("{0}-{1}", cont++, song.nombre));
                }
                Console.WriteLine(String.Format("[{0}]-{1}", cont++, "REGRESAR"));

                Console.WriteLine("Escriba un número para ver el nombre del artista de la canción:");
                string opc = Console.ReadLine();
                int respuesta = 0;
                while (!int.TryParse(opc, out respuesta) || respuesta > cont - 1)
                {
                    Console.WriteLine("Opción inválida, intente de nuevo");
                    opc = Console.ReadLine();
                }
                if (respuesta == cont - 1)
                {
                    Console.Clear();
                    await showAlbumsArtista(selectedArtista);
                }
                else
                {
                    selectedCancion = lstCanciones[Convert.ToInt32(opc) - 1];
                    await showArtistaTrack(selectedCancion);
                }
                
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Ocurrió un error, NAPSTER se cerrará al presionar una tecla");
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        static async Task showArtistaTrack(Cancion cancion)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url_base + "/artistbysong?track=" + cancion.href);

                String artista = await response.Content.ReadAsStringAsync();

                Console.WriteLine("---- Artista de la cacnion " + cancion.nombre + " -> [" + artista.ToUpper() + "]");

                Console.ReadLine();

                Console.Clear();
                await showGeneros();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Ocurrió un error, NAPSTER se cerrará al presionar una tecla");
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }


    }
}
